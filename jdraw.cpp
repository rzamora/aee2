#include "jdraw.h"
#include <SDL.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace jdraw {

	int screen_width = 0;
	int screen_height = 0;

	SDL_Window* sdlWindow = NULL;
	SDL_Renderer* sdlRenderer = NULL;

	void init(const char* title, const int width, const int height, const int zoom) {
		screen_width = width;
		screen_height = height;
		sdlWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width * zoom, height * zoom, SDL_WINDOW_SHOWN);
		sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);
		SDL_SetRenderDrawBlendMode(sdlRenderer, SDL_BLENDMODE_BLEND);
		SDL_RenderSetLogicalSize(sdlRenderer, width, height);
	}

	void quit() {
		SDL_DestroyRenderer(sdlRenderer);
		SDL_DestroyWindow(sdlWindow);
	}

	void clear() {
		SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255); SDL_RenderClear(sdlRenderer);
	}

	void flip() {
		SDL_RenderPresent(sdlRenderer);
	}

	void set_render_target(jtexture texture) {
		if (texture != nullptr) {
			SDL_SetRenderTarget(sdlRenderer, texture->texture);
		} else {
			SDL_SetRenderTarget(sdlRenderer, nullptr);
		}
	}

	void set_blend_mode(jtexture texture, blend_mode mode) {
		SDL_SetTextureBlendMode(texture->texture, SDL_BlendMode(mode));
	}

	jtexture createTexture(const int width, const int height) {
		jtexture texture = (jtexture)malloc(sizeof(jtexture_t));
		texture->texture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, width, height);
		SDL_SetTextureBlendMode(texture->texture, SDL_BLENDMODE_BLEND);
		return texture;
	}

	jtexture load_texture(const char* filename) {
		int c;
		jtexture texture = (jtexture)malloc(sizeof(jtexture_t));
		FILE* f = fopen(filename, "rb");
		if (!f) { SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, filename, "File is missing!! Meim is petted!!!", nullptr); return nullptr; }
		Uint8* buffer = stbi_load_from_file(f, &texture->w, &texture->h, &c, 4);
		texture->texture = SDL_CreateTexture(sdlRenderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STATIC, texture->w, texture->h);
		SDL_UpdateTexture(texture->texture, nullptr, buffer, texture->w * sizeof(Uint32));
		SDL_SetTextureBlendMode(texture->texture, SDL_BLENDMODE_BLEND);
		stbi_image_free(buffer);
		fclose(f);
		return texture;
	}

	void free_texture(jtexture texture) {
		SDL_DestroyTexture(texture->texture);
		free(texture);
	}

	void draw_tile(jtexture texture, int x, int y, int sx, int sy, int w, int h) {
		static SDL_Rect src, dst;
		src.x = sx; src.y = sy;
		dst.x = x; dst.y = y;
		src.w = dst.w = w;
		src.h = dst.h = h;
		SDL_RenderCopy(sdlRenderer, texture->texture, &src, &dst);
	}

	void draw(jtexture texture, int x, int y, int sx, int sy, int w, int h, SDL_RendererFlip flip) {
		static SDL_Rect src, dst;
		src.x = sx; src.y = sy;
		dst.x = x; dst.y = y;
		src.w = dst.w = w;
		src.h = dst.h = h;
		static SDL_Point p;
		p.x = p.y = 0;
		SDL_RenderCopyEx(sdlRenderer, texture->texture, &src, &dst, 0, &p, flip);
	}

	void draw2(jtexture texture, int x, int y, int sx, int sy, int w, int h, int sw, int sh, SDL_RendererFlip flip) {
		static SDL_Rect src, dst;
		src.x = sx; src.y = sy;
		dst.x = x; dst.y = y;
		src.w = sw;
		dst.w = w;
		src.h = sh;
		dst.h = h;
		static SDL_Point p;
		p.x = p.y = 0;
		SDL_RenderCopyEx(sdlRenderer, texture->texture, &src, &dst, 0, &p, flip);
	}

}