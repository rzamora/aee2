#include "jmap.h"
#include <stdio.h>

#include "jdraw.h"

namespace jmap {

	int x, y;						// map position in the screen (in pixels)
	int room_width, room_height;	// room size in tiles
	int map_width, map_height;		// map size in tiles
	int tile_width, tile_height;	// tile size in pixels
	int room;						// current room
	int room_x, room_y;				// current room's position in the map (in tiles)
	int rooms_per_width;			// how many rooms fit the map horizontally
	int tiles_per_width;			// how many tiles fit the tiles texture horizontally
	Uint8* data = nullptr;			// raw tile data
	jtexture tiles = nullptr;		// texture that holds the tiles

	void init(const char* filename, jtexture tiles, const int tile_width, const int tile_height) {
		jmap::tiles = tiles;
		jmap::tile_width = tile_width;
		jmap::tile_height = tile_height;

		FILE* f = fopen(filename, "rb");
			fread(&map_width, 1, 1, f);
			fread(&map_height, 1, 1, f);
			if (map_width == 0) map_width = 256;
			data = (Uint8*)calloc(map_width * map_height, 1);
			fread(data, map_width*map_height, 1, f);
		fclose(f);

		x = y = room_x = room_y = room = 0;
		room_width = jdraw::screen_width / tile_width;
		room_height = jdraw::screen_height / tile_height;
		rooms_per_width = map_width / room_width;
		tiles_per_width = tiles->w / tile_width;
	}

	void quit() {
		if (data) free(data);
		data = nullptr;
	}

	void set_position(const int x, const int y) {
		jmap::x = x;
		jmap::y = y;
	}

	void set_room_size(const int w, const int h) {
		room_width = w;
		room_height = h;
		rooms_per_width = map_width / room_width;
	}

	void calculate_room() {
		room = (room_x / room_width) + (room_y / room_height) * rooms_per_width;
	}

	const int get_room() {
		return room; // (room_x / room_width) + (room_y / room_height) * rooms_per_width;
	}

	void set_room(const int room) {
		jmap::room = room;
		room_x = (room % rooms_per_width)*room_width;
		room_y = (room / rooms_per_width)*room_height;
	}

	void move_room(const int direction) {
		switch (direction) {
		case ROOM_UP:		room_y -= room_height; calculate_room(); break;
		case ROOM_RIGHT:	room_x += room_width;  calculate_room(); break;
		case ROOM_DOWN:		room_y += room_height; calculate_room(); break;
		case ROOM_LEFT:		room_x -= room_width;  calculate_room(); break;
		default: break;
		}
	}

	const Uint8 get_tile(const int x, const int y) {
		return data[(room_x + x) + (room_y + y) * map_width];
	}

	const Uint8 get_tile_in_pos(const int x, const int y) {
		return get_tile(x / tile_width, y / tile_height);
	}

	void set_tile(const int x, const int y, const Uint8 tile) {
		data[(room_x + x) + (room_y + y) * map_width] = tile;
	}

	void draw() {
		for (int j = 0; j < room_height; j++) {
			for (int i = 0; i < room_width; i++) {
				const Uint8 tile = get_tile(i, j);
				const int tile_x = (tile % tiles_per_width)*tile_width;
				const int tile_y = (tile / tiles_per_width)*tile_height;
				jdraw::draw_tile(tiles, x + (i*tile_width), y + (j*tile_height), tile_x, tile_y, tile_width, tile_height);
			}
		}
	}

}
