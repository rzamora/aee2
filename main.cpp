#include "jgame.h"
//#include "jmap.h"

#include <stdlib.h> // rand()

jtexture background		= nullptr;
jtexture darkness		= nullptr;
jtexture light			= nullptr;

bool test_state();

int x = 55;
int y = 45;

bool test_init() {
	background = jdraw::load_texture("test_background.png");
	light = jdraw::load_texture("test_light2.png");
	jdraw::set_blend_mode(light, jdraw::add);
	darkness = jdraw::createTexture(160, 120);
	jdraw::set_blend_mode(darkness, jdraw::mod);
	jdraw::set_render_target(darkness);
	jdraw::clear();
	jdraw::set_render_target(nullptr);

	jinput::init();

	jgame::go(test_state);
	return KEEP_GOING;
}

bool test_state() {
	if (jinput::keys[SDL_SCANCODE_UP]) { y--; }
	if (jinput::keys[SDL_SCANCODE_DOWN]) { y++; }
	if (jinput::keys[SDL_SCANCODE_LEFT]) { x--; }
	if (jinput::keys[SDL_SCANCODE_RIGHT]) { x++; }

	int coef = (rand() % 6) - 3;
	jdraw::set_render_target(darkness);
	jdraw::clear();
	jdraw::draw2(light, x - 50, y - 50, 0, 0, 100 + coef, 100 + coef, 64, 64);
	jdraw::draw2(light, 93 - 50, 70 - 50, 0, 0, 100 + coef, 100 + coef, 64, 64);
	jdraw::set_render_target(nullptr);

	jdraw::clear();
	jdraw::draw(background, 0, 0, 0, 0, 160, 120);
	jdraw::draw(darkness, 0, 0, 0, 0, 160, 120);
	jdraw::flip();

	return KEEP_GOING;
}

int main(int argc, char* argv[]) {
	jgame::init("AEE2 v0.01", 160, 120, 4);
	jgame::run(test_init);

	return 0;
}