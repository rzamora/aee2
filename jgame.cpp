#include "jgame.h"
#include <SDL.h>

#include "jdraw.h"
#include "jinput.h"

namespace jgame {

	LoopFun currentLoop;

	void init(const char* title, const int width, const int height, const int zoom) {
		SDL_Init(SDL_INIT_EVERYTHING);
		jdraw::init(title, width, height, zoom);
		jinput::init();
	}

	void quit() {
		jdraw::quit();
		SDL_Quit();
	}

	void run(LoopFun loop) {
		static bool should_quit = false;
		static SDL_Event sdlEvent;

		currentLoop = loop;

		while (!should_quit) {
			jinput::just_pressed = SDL_SCANCODE_UNKNOWN;
			while (SDL_PollEvent(&sdlEvent)) {
				if (sdlEvent.type == SDL_QUIT) { should_quit = true; break; }
				else if (sdlEvent.type == SDL_KEYDOWN) {
					//anyKey = true;
					jinput::just_pressed = sdlEvent.key.keysym.scancode;
					if (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE) { should_quit = true; }
				}
			}
			if (!should_quit) should_quit = currentLoop();
		}
		quit();
	}

	void go(LoopFun loop) {
		currentLoop = loop;
	}

}
