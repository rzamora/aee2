#pragma once

#include "jdraw.h"
#include "jinput.h"

#define KEEP_GOING false
#define EXIT_GAME true

typedef bool(*LoopFun)(void);

namespace jgame {

	void init(const char* title, const int width, const int height, const int zoom);
	void quit();

	void run(LoopFun loop);
	void go(LoopFun loop);

}
