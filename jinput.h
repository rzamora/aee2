#pragma once

#include <SDL.h>

namespace jinput {

	extern const Uint8* keys;
	extern Uint8 just_pressed;

	void init();

}
