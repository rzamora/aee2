#pragma once

#include "jdraw.h"

#define ROOM_UP 0
#define ROOM_RIGHT 1
#define ROOM_DOWN 2
#define ROOM_LEFT 3

namespace jmap {

	void init(const char* filename, jtexture tiles, const int tile_width, const int tile_height);
	void quit();

	void set_position(const int x, const int y);
	void set_room_size(const int w, const int h);

	const int get_room();
	void set_room(const int room);
	void move_room(const int direction);

	const Uint8 get_tile(const int x, const int y);
	const Uint8 get_tile_in_pos(const int x, const int y);
	void set_tile(const int x, const int y, const Uint8 tile);

	void draw();

}
