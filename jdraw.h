#pragma once
#include <SDL.h>

struct jtexture_t {
	SDL_Texture*	texture;
	int				w, h;
};

typedef jtexture_t* jtexture;

namespace jdraw {

	enum blend_mode {
		none	= 0,
		blend	= 1,
		add		= 2,
		mod		= 4
	};

	extern int screen_width;
	extern int screen_height;

	void init(const char* title, const int width, const int height, const int zoom);
	void quit();

	void clear();
	void flip();
	
	void set_render_target(jtexture texture);
	void set_blend_mode(jtexture texture, blend_mode mode);

	jtexture createTexture(const int width, const int height);
	jtexture load_texture(const char* filename);
	void free_texture(jtexture texture);

	void draw_tile(jtexture texture, int x, int y, int sx, int sy, int w, int h);
	void draw(jtexture texture, int x, int y, int sx, int sy, int w, int h, SDL_RendererFlip flip = SDL_FLIP_NONE);
	void draw2(jtexture texture, int x, int y, int sx, int sy, int w, int h, int sw, int sh, SDL_RendererFlip flip = SDL_FLIP_NONE);

}